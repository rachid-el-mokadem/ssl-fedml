# Federated Learning communications optimization using Sparse Single-layer updates

To change experimentation parameters,  the following variables can be adapted in the code:
```
epochs = 40 # number of client iterations
num_workers = 18 # number of client nodes 
rounds=100 # number of communication rounds
MIN_CLASS_N=10 # number of max classes per node
MIN_CLASS_N=10 # number of min classes per node
batch_size=5 # batch size of nodes training dataset 
layers_update_num=1 # number of layer to send to the server
sparsity_k=0.1 # the sparsity ratio
```
To run the experimentation, use the following command:

```
python SparseFedML.py
```