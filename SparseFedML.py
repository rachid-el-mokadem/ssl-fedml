import torch
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.utils.data.sampler import SubsetRandomSampler
import time, os
import numpy as np
import math
import itertools
import statistics
from collections import Counter
import csv
import random
from itertools import chain

SEED = 10

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed_all(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

__all__ = [
    'VGG', 'vgg11', 'vgg11_bn', 'vgg13', 'vgg13_bn', 'vgg16', 'vgg16_bn',
    'vgg19_bn', 'vgg19',
]


class VGG(nn.Module):
    '''
    VGG model
    '''
    def __init__(self, features):
        super(VGG, self).__init__()
        self.features = features
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 10),
        )
         # Initialize weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                m.bias.data.zero_()


    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x


def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


cfg = {
    'X': [64, 'M', 128, 'M', 256, 'M', 512, 'M', 512, 512, 'M'],
    'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M',
          512, 512, 512, 512, 'M'],
}

def vgg9():
    """VGG 11-layer model (configuration "A")"""
    return VGG(make_layers(cfg['X']))

def vgg9_bn():
    """VGG 11-layer model (configuration "A")"""
    return VGG(make_layers(cfg['X'], batch_norm=True))

def vgg11():
    """VGG 11-layer model (configuration "A")"""
    return VGG(make_layers(cfg['A']))


def vgg11_bn():
    """VGG 11-layer model (configuration "A") with batch normalization"""
    return VGG(make_layers(cfg['A'], batch_norm=True))


def vgg13():
    """VGG 13-layer model (configuration "B")"""
    return VGG(make_layers(cfg['B']))


def vgg13_bn():
    """VGG 13-layer model (configuration "B") with batch normalization"""
    return VGG(make_layers(cfg['B'], batch_norm=True))


def vgg16():
    """VGG 16-layer model (configuration "D")"""
    return VGG(make_layers(cfg['D']))


def vgg16_bn():
    """VGG 16-layer model (configuration "D") with batch normalization"""
    return VGG(make_layers(cfg['D'], batch_norm=True))


def vgg19():
    """VGG 19-layer model (configuration "E")"""
    return VGG(make_layers(cfg['E']))


def vgg19_bn():
    """VGG 19-layer model (configuration 'E') with batch normalization"""
    return VGG(make_layers(cfg['E'], batch_norm=True))

class NetC(nn.Module):
    def __init__(self):
        super(NetC, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 16, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(16, 32, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(32, 64, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(64, 128, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2)
        )
        self.copy=None
        self.classifiers = nn.Sequential(
            nn.Linear(2 * 2 * 128, 256),
            nn.ReLU(inplace=True),
            nn.Linear(256, 10)
        )
    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 2*2*128)
        self.dropout = nn.Dropout(0.25)
        x = self.classifiers(x)
        return x

class NetB(nn.Module):
    def __init__(self):
        super(NetB,self).__init__()
        # Convolutional Layer
        self.conv1 = nn.Conv2d(3,16,3, padding=1)
        self.conv2 = nn.Conv2d(16,32,3,padding=1)
        self.conv3 = nn.Conv2d(32,64,3,padding=1)
        self.pool = nn.MaxPool2d(2,2)
        self.fc1 = nn.Linear(64*4*4,120)
        self.fc2 = nn.Linear(120, 60)
        self.fc3 = nn.Linear(60,10)
        self.dropout = nn.Dropout()

    def forward(self, x):

        x =  self.pool(F.relu(self.conv1(x)))
        x =  self.pool(F.relu(self.conv2(x)))
        x =  self.dropout(x)
        x =  self.pool(F.relu(self.conv3(x)))
        x =  x.view(-1,64*4*4)
        x =  self.dropout(x)
        x = F.relu(self.fc1(x))
        x =  self.dropout(x)
        x = F.relu(self.fc2(x))
        x =  self.dropout(x)
        x = self.fc3(x)
        return x

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 10, kernel_size=5)  # Change number of input channels to 3
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(500, 50)  # Change the input size of the first fully connected layer
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 500)  # Change the input size of the first fully connected layer
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)
#@title Define Hyperparameters

input_size = 784 # img_size = (28,28) ---> 28*28=784 in total
hidden_size = 500 # number of nodes at hidden layer
num_classes = 10 # number of output classes discrete range [0,9]
num_epochs = 20 # number of times which the entire dataset is passed throughout the model
batch_size = 100 # the size of input data took for one iteration
lr = 1e-3 # size of step
class ModelC(nn.Module):
    '''
    VGG model
    '''
    X= [64, 'M', 128, 'M', 256, 'M', 512, 'M', 512, 512, 'M']

    def make_layers(cfg, batch_norm=False):
        layers = []
        in_channels = 3
        for v in cfg:
            if v == 'M':
                layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
            else:
                conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
                if batch_norm:
                    layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
                else:
                    layers += [conv2d, nn.ReLU(inplace=True)]
                in_channels = v
        return nn.Sequential(*layers)

    def __init__(self):
        super(ModelC, self).__init__()
        self.features = make_layers(X)
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Linear(512, 10),
        )
         # Initialize weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                m.bias.data.zero_()


    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

class ModelB(nn.Module):
    def __init__(self):
        super(ModelB, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 16, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(16, 32, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(32, 64, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(64, 128, 3, 1, 1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2)
        )
        self.copy=None
        self.classifiers = nn.Sequential(
            nn.Linear(2 * 2 * 128, 256),
            nn.ReLU(inplace=True),
            nn.Linear(256, 10)
        )
    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 2*2*128)
        self.dropout = nn.Dropout(0.25)
        x = self.classifiers(x)
        return x

class ModelA(nn.Module):
    def __init__(self):
        super(ModelA, self).__init__()
        self.conv1 = nn.Conv2d(3, 10, kernel_size=5)  # Change number of input channels to 3
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(500, 50)  # Change the input size of the first fully connected layer
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 500)  # Change the input size of the first fully connected layer
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)
class FNet(nn.Module):
  def __init__(self, input_size, hidden_size, num_classes):
    super(FNet,self).__init__()
    self.fc1 = nn.Linear(input_size, hidden_size)
    self.relu = nn.ReLU()
    self.fc2 = nn.Linear(hidden_size, num_classes)

  def forward(self,x):
    out = self.fc1(x)
    out = self.relu(out)
    out = self.fc2(out)
    return out
class SimpleModel(nn.Module):
    def __init__(self):
        super(SimpleModel, self).__init__()
        self.fc1 = nn.Linear(1, 10)
        self.fc2 = nn.Linear(10, 1)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)
        return x
def getRandomClasses(n_classes, n_workers):
  np.random.seed(321)
  hMp={}
  classes=[]
  class_num_mean=0
  if isinstance(n_classes,list):
    class_num_mean=math.ceil(statistics.mean(n_classes))
  else:
    class_num_mean=n_classes
  cls_bag=[i%10 for i in range(class_num_mean*n_workers)]
  for w in range(n_workers):
    n_c=[]
    for c in range(n_classes[w]):
      cls=np.random.choice(cls_bag,1)
      while((cls in n_c)):
        cls=np.random.choice(cls_bag,1)
        if (len(cls_bag) == 1):
            break
      n_c.append(cls[0])
      cls_bag.remove(cls[0])
    classes.append(n_c)
  flat_list = [item for sublist in classes for item in sublist]
  return classes, Counter(flat_list)

def split_dataset_by_label(trainset, clients_classes, counter):
  clients_indices=[]
  clas_tracker={0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0,8:0,9:0}
  targets_indices = [np.where((np.array(trainset.targets) == i))[0] for i in range(10)]
  for i,client in enumerate(clients_classes):
    client_idx=[]
    for cls in client:
      fraction=len(targets_indices[cls])//counter[cls]
      client_idx.append(targets_indices[cls][clas_tracker[cls]*fraction:(clas_tracker[cls]+1)*fraction-1])
      clas_tracker[cls]+=1
    client_split =  [item for sublist in client_idx for item in sublist]
    #random.shuffle(client_split)
    clients_indices.append(client_split)
  return clients_indices


def worker1(rank, gpu_id, input_queue, output_queue, data_loader, test_loader, avg_params, epochs, rounds, number_layers, sparsity_k):
    # Initialize the model and move it to the GPU
    device = torch.device(f'cuda:{gpu_id}')
    model = ModelB().to(device)
    model.load_state_dict(avg_params)
    #avg_params={name: param.detach() for name, param in model.named_parameters()}
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.01)
    #epoch = 0
    for round in range(rounds):
        start_time = time.time()
        for epoch in range(epochs):
            train_epoch(model, round, epoch, data_loader, optimizer, criterion, device)
        model_weights={name:param.detach() for name, param in model.named_parameters()}
        # Move the model parameters back to the CPU before putting them in the output queue avg_params[name].cuda(rank%2) -
        #delta_weights={name: param.detach() for name, param in model.named_parameters()}

        # Get all named parameters and group them by layer
        layer_params = {}
        for name, param in model.named_parameters():
            # Remove the last part of the name (".weight" or ".bias")
            layer_name = '.'.join(name.split('.')[:-1])
            if layer_name not in layer_params:
                layer_params[layer_name] = []
            layer_params[layer_name].append((name, param.detach()))

       # Compute the layer index for this round
        layer_index = (round+rank) % len(layer_params)

        # Get a list of layer names
        layer_names = list(layer_params.keys())

        # Get the selected layer names with wrapping around
        selected_layer_names = [layer_names[i % len(layer_names)] for i in range(layer_index, layer_index+number_layers)]

        # Now you have a list of selected layer names, you can get their parameters
        selected_layer_params = list(chain(*[layer_params[name] for name in selected_layer_names]))


        print("rank",rank,"selected layer's index",selected_layer_names)
        delta_weights = {name:get_sparse_grad(avg_params[name].to(device) - param.to(device), sparsity_k)  for name,param in selected_layer_params}


        output_queue.put((gpu_id, delta_weights, model_weights))
        time.sleep(0.01)
        #test(model, torch.device('cuda'), test_loader)
        avg_params = input_queue.get()
        model.load_state_dict(avg_params)
        #avg_params={name: param.detach() for name, param in model.named_parameters()}
        end_time = time.time()  # track the end time of the epoch
        epoch_time = end_time - start_time  # compute the epoch time
        print('Worker {} time taken: {:.2f} seconds'.format(rank, epoch_time))
def get_flat_sparse_indices(indices):
  indices_list=[]
  indices_list.append(list(np.repeat(0, len(indices))))
  indices_list.append(list(indices.cpu().numpy()))
  return indices_list
def get_sparse_indices(indices, size, k):
  indices_len=len(indices)
  indices_list=[]
  for idx,s in enumerate(size):
    if idx == len(size)-1:
      indices_list.append(list(indices))
    else:
      t=np.repeat(np.arange(s),indices_len/s/(idx+1))
      indices_list.append(np.tile(t,idx+1))
  return np.array(indices_list)
def get_sparse_tensor(input,k):
  sign_input=input.sign()
  top_k_tensor=input.abs().flatten().topk(k)
  x_i=get_flat_sparse_indices(top_k_tensor.indices)
  #print(x_i)
  x_v=top_k_tensor.values.flatten()
  return torch.sparse_coo_tensor(x_i,x_v, size=(1,len(input.flatten()))).to_dense().view(input.size())*sign_input


def get_sparse_grad(t,sparsity_k):
    top_k=math.floor(len(t.flatten())*sparsity_k)
    sparse_d_w=get_sparse_tensor(t,top_k)
    return sparse_d_w

def train_epoch(model, round, epoch, data_loader, optimizer, criterion, device):
    model.train()
    pid = os.getpid()
    for batch_idx, x in enumerate(data_loader):
        data, target = x[0].to(device), x[1].to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)  # Use cross-entropy loss for classification tasks
        loss.backward()
        optimizer.step()

        '''if batch_idx % 100== 0:
            print('{}\tRank {} Round {} Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                pid, pid, round, epoch, batch_idx * len(data), len(data_loader.dataset),
                100. * batch_idx / len(data_loader), loss.item()))'''

def test(model, round, device, data_loader):
    model.eval()
    model= model.to(device)
    test_loss = 0
    correct = 0
    criterion = nn.CrossEntropyLoss()
    with torch.no_grad():
        for batch_idx, x in enumerate(data_loader):
            data, target = x[0].to(device), x[1].to(device)
            output = model(data)
            test_loss += criterion(output, target).item() # sum up batch loss
            pred = output.max(1)[1] # get the index of the max log-probability
            correct += pred.eq(target).sum().item()

    test_loss /= len(data_loader.dataset)
    accuracy = 100. * correct / len(data_loader.dataset)
    print('\nTest round {}: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(
        round, test_loss, correct, len(data_loader.dataset),
        accuracy))

    return accuracy  # return accuracy

def Net_from_weights(model):
  net=ModelB()
  net.load_state_dict(model.state_dict())
  return net

def _init_fn(worker_id):
    np.random.seed(SEED)

def getDataLoader(subset):
    train_sampler = SubsetRandomSampler(subset.indices)
    train_loader = torch.utils.data.DataLoader(
        subset.dataset,
        sampler=train_sampler,
        worker_init_fn=_init_fn
    )
    return train_loader

def avg_grads(w_grads, avg_model, device):
    gradients = {n:torch.zeros_like(param).to(device) for n, param in avg_model.named_parameters()}
    num_workers=len(w_grads)
    for i, w_grad in enumerate(w_grads):
        #print(i, w_grad['conv1.bias'][0])
        for j, param_grad in enumerate(w_grad):
            gradients[param_grad] += w_grad[param_grad]
    avg_gradient = {name: (grad / num_workers).detach() for name, grad in gradients.items()}
    return avg_gradient


def save_model_checkpoint(model, path):
    torch.save(model.state_dict(), path)

def load_checkpoint(model, optimizer, filename='checkpoint.pth.tar'):
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename)
        #start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})"
                  .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer, start_epoch
def get_size_of_updates(updates):
    total_elements = sum(param.numel() for param in updates.values())
    # Convert the size to bytes
    total_size_bytes = total_elements * 4  # a float is 4 bytes
    # Convert the size to kilobytes
    total_size_kb = total_size_bytes / 1024
    # Convert the size to megabytes
    total_size_mb = total_size_kb / 1024
    return total_size_mb  # returns size in MB


def main():

    epochs = 40
    num_workers = 18
    rounds=500
    MIN_CLASS_N=10
    MAX_CLASS_N=10
    batch_size=5
    layers_update_num=1
    sparsity_k=0.1
    rnd_class_num = sorted([np.random.randint(MIN_CLASS_N, MAX_CLASS_N+1) for a in range(num_workers)],reverse=True)

    # Set the start method for multiprocessing
    mp.set_start_method('spawn')
    model = avg_model = ModelB()
    input_queue = mp.SimpleQueue()
    output_queue = mp.SimpleQueue()

    # Check if CUDA is available
    if not torch.cuda.is_available():
        raise RuntimeError("CUDA is not available on this system.")

    device = torch.device('cuda')

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))  # Update the normalization constants
    ])
    dataset1 = datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    dataset2 = datasets.CIFAR10(root='./data', train=False, transform=transform)

    test_loader = torch.utils.data.DataLoader(dataset2)
    kwargs = {'batch_size': batch_size}

    classes,counter=getRandomClasses(rnd_class_num, num_workers)
    partitions=split_dataset_by_label(dataset1, classes, dict(counter))


    train_loaders=[torch.utils.data.DataLoader(torch.utils.data.Subset(dataset1, partitions[i]), batch_size=batch_size, shuffle=True) for i in range(num_workers)]
    models=[Net_from_weights(model) for a in range(num_workers)]
    # Spawn worker processes
    processes = []

    num_gpus = torch.cuda.device_count()

    for i in range(num_workers):
        gpu_id = i % num_gpus  # This will assign GPUs round-robin style
        p = mp.Process(target=worker1, args=(i, gpu_id, input_queue, output_queue, train_loaders[i], test_loader, models[i].state_dict(), epochs, rounds, layers_update_num, sparsity_k))
        p.start()
        processes.append(p)


    # Signal the workers to exit
    '''for _ in range(num_workers):
        input_queue.put(None)'''

    # Collect updated model parameters from the output queue
    w_grads = []
    test_accuracies = []  # initialize an empty list to store test accuracies

    for round in range(rounds):
        total_update_size = 0.0
        gpus_grads={}
        avg_weights = avg_model.state_dict()
        gradients = {n:torch.zeros_like(param) for n, param in avg_model.named_parameters()}
        start_time = time.time()

        for _ in range(num_workers):
            device_rnk, w_delta, w_grad = output_queue.get()
            update_size = get_size_of_updates(w_delta)
            total_update_size += update_size
            if device_rnk not in gpus_grads:
                gpus_grads[device_rnk]=[]
            gpus_grads[device_rnk].append(w_delta)
            #w_grads.append(w_grad)
        end_time = time.time()  # track the end time of the epoch
        round_time = end_time - start_time  # compute the epoch time
        print('Round {} time taken: {:.2f} seconds'.format(round, round_time))

        for n in gpus_grads:
            gradients_temp=avg_grads(gpus_grads[n], avg_model, torch.device("cuda",n))
            gradients = {name: gradients[name].to(torch.device("cuda",n)) + param.detach() for name, param in gradients_temp.items()}
        avg_delta = {name: (grad / len(gpus_grads)).detach().cpu() for name, grad in gradients.items()}

        #avg_delta=avg_grads(gpus_grads[0], avg_model, torch.device("cuda",0))
        #print(avg_delta.keys())
        #print("avg", avg_delta['conv1.bias'][0])
        for a in avg_delta:
            avg_weights[a].copy_(avg_weights[a].to(device) - avg_delta[a].to(device) )
        avg_model.load_state_dict(avg_weights)
        print("Size of the updates: {:.2f} MB".format(total_update_size))
        accuracy = test(avg_model, round,  device, test_loader)  # get accuracy from the test() function
        test_accuracies.append(accuracy)  # append the accuracy to the test_accuracies list

        checkpoint_path = "./global_model_checkpoint.pth"
        save_model_checkpoint(avg_model, checkpoint_path)

        for _ in range(num_workers):
            avg={name: param.detach() for name, param in avg_model.named_parameters()}
            input_queue.put(avg)

        with open('test_accuracies.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            if round == 0:  # if it's the first time we're writing to the file, include the header
                writer.writerow(['Round', 'Accuracy'])
            writer.writerow([round, accuracy])

    # Join worker processes
    for p in processes:
        p.join()


if __name__ == '__main__':
    main()
